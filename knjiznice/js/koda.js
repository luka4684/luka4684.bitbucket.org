
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var vsiZdravniki = [];
var json;
// seznam z markerji na mapi
var markerji = [];
var mapa;
const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {
  // TODO: Potrebno implementirati
  $("#preveriSpanec").empty();
  $("#preveriSpanec").append('<option value=""</option>');
  var ime = "Janez";
  var priimek = "Novak";
  var datumRojstva = "1980-05-20"
  var visina = 180;
  var teza = 80;
  var sistolicni = 118;
  var diastolicni = 92;
  var spanec = 8;
  var prebujanje = "ne";
  EHRzaTestnePaciente(ime, priimek, datumRojstva, visina, teza, sistolicni, diastolicni, spanec, prebujanje);
  
  ime = "Donald";
  priimek = "Trump";
  datumRojstva = "1960-06-14"
  visina = 170;
  teza = 120;
  sistolicni = 150;
  diastolicni = 200;
  spanec = 12;
  prebujanje = "ja";
  EHRzaTestnePaciente(ime, priimek, datumRojstva, visina, teza, sistolicni, diastolicni, spanec, prebujanje);
  
  ime = "Archie";
  priimek = "Harrison";
  datumRojstva = "2019-05-06"
  visina = 55;
  teza = 3;
  sistolicni = 80;
  diastolicni = 60;
  spanec = 9;
  prebujanje = "ja";
  EHRzaTestnePaciente(ime, priimek, datumRojstva, visina, teza, sistolicni, diastolicni, spanec, prebujanje);

}

function EHRzaTestnePaciente(ime, priimek, datumRojstva, visina, teza, sistolicni, diastolicni, spanec, prebujanje) {
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#preveriSpanec").append('<option id="'+ime+priimek+'" value="'+ehrId+"|"+visina+"|"+teza+"|"+sistolicni+"|"+diastolicni+"|"+spanec+"|"+prebujanje+'">'+ ime + " " + priimek +'</option>');
              alert(ime +" "+ priimek + " ima kreiran EHR: "+ehrId+ ". Njegove podatke lahko najdeš v seznamu zraven 'Preveri kvaliteto svojega spanca'");
            }
          },
          error: function(err) {
          	alert("Prišlo je do napake!");
          }
        });
      }
		});
}

function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite vse potrebne podatke</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>" + ime + " " + priimek + " ima uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
              $("#preveriSpanec").append('<option value="'+ehrId+'">'+ ime + " " + priimek +'</option>');
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}

function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
    {
        age--;
    }
    return age;
}

function kvalitetaSpanja() {
	var ehrId = $("#dodajVitalnoEHR").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var casSpanja = $("#dodajCasSpanja").val();
	var prebuditev = 0;
	if (document.getElementById("prebujanje").checked == true) {
	  prebuditev = 1;
	}
	document.getElementById("skritiNevrologi").style.visibility = "hidden";
	

	var starost;
	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		type: 'GET',
		headers: {
      "Authorization": getAuthorization()
    },
  	success: function (data) {
  	  var party = data.party;
			starost = getAge(party.dateOfBirth);
			var rezultat = 100;
			if (starost < 1) {
			  if (casSpanja <= 10) {
			    rezultat = 15;
			    $("#analizaSpanja").html("<p><mark>Opozorilo!</mark></p> Nacionalna fundacija za spanec je podala nove smernice, koliko bi moral vsak od nas spati. " +
			    "Dolžina potrebnega spanca je odvisna od naše starosti. Na točnih priporočilih za novorojenčke in starejše so delali znanstveniki kar dve leti, pri projektu pa so sodelovali tudi ugledneži z Ameriške akademije za pediatrijo in Ameriškega združenja za geriatrijo. " +
          "Dojenčki morajo spati <b>več kot 10 ur</b>. Največ morajo spati novorojenčki oziroma dojenčki <b>do tretjega meseca starosti</b>. Ti naj bi skupno spali od <b>14 do 17 ur na dan</b>, <b>najnižja meja</b> mora biti <b>11</b>, najvišja pa <b>19 ur na dan</b>. Nekoliko manj spijo dojenčki od <b>četrtega do enajstega meseca</b>, in sicer od <b>12 do 15 ur na dan</b>. Sem štejemo seveda vse, tudi popoldanske spance. <b>Najnižja meja je 10, najvišja pa 18 ur</b>. " +
          "<p class='bg-danger text-white'><b>Glede na vaš odgovor, resno ogrožate otrokovo življenje, zato priporočamo pogovor z zdravnikom.</b></p>");
          document.getElementById("skritiNevrologi").style.visibility = "visible";
			  }
			  else {
			    $("#analizaSpanja").html("<p class='bg-success text-white'>Ste odgovoren starš oz. skrbnik, saj vaš dojenček spi več kot 10 ur, kar je v skladu z Nacionalno fundacijo za spanec. Le tako naprej.</p>");
			  }
			}
			if (starost >= 1 && starost <=2) {
			  if (casSpanja <= 10) {
			    rezultat = 15;
			    $("#analizaSpanja").html("Vašemu malčku primanjkuje spanja. Če želite, da zraste v močnega, zdravega ... človeka, mora spati dlje, saj Nacionalna fundacija za spanec priporoča zanje od <b>11 do 14 ur</b> spanja. " +
			    "<p class='bg-danger text-white'><b>Glede na vaš odgovor in starost vašega otroka, vam priporočamo pogovor z zdravnikom.</b></p>");
			    document.getElementById("skritiNevrologi").style.visibility = "visible";
			  }
			  else {
			    $("#analizaSpanja").html("<p class='bg-success text-white'><b>Vaš malček je deležen dovolj spanja, le tako naprej.</b></p>");
			  }
			}
			if (starost >= 3 && starost <= 5) {
			  if (casSpanja < 10) {
			    rezultat = 25;
			    $("#analizaSpanja").html("Vaš otrok še odrašča, star je komaj <b>" + starost + "</b>, pa spi le <b>" + casSpanja +"</b>. ur dnevno. Če želite, da je vaš otrok spočit in zdrav, bi moral spati <b>med 10 in 13 ur</b>. " +
			    "<p class='bg-danger text-white'><b>Glede na vaš odgovor in starost vašega otroka, vam priporočamo pogovor z zdravnikom.</b></p>");
			    document.getElementById("skritiNevrologi").style.visibility = "visible";
			  }
			  else {
			    $("#analizaSpanja").html("Glede na to, da je vaš otrok star <b>" + starost + "</b>, je število ur spanja ravno pravšnje." +
			    "<p class='bg-success text-white'>Ste skrben starš, le tako naprej</p>");
			  }
			}
			if (starost <= 13 && starost > 5) {
			  if (casSpanja == 7) {
			    rezultat = 50;
			    $("#analizaSpanja").html("Do trinajstega leta bi morali otroci spati med <b>devet in enajst ur na dan</b>, vaš otrok pa <b>spi 7 ur</b>, kar je ravno spodnja skrajna meja." +
			    "<p class='bg-info text-white'><b>Priporočamo vam, da spite kakšno uro dlje, tako da boste znotraj priporočenega intervala</b>.</p>");
			  }
			  else if (casSpanja < 7) {
			    rezultat = 25;
			    $("#analizaSpanja").html("Spite le <b>" + casSpanja + "</b>. ur, pri <b>" + starost + "</b>. letih. Želite postati depresiven?" +
			    "<p class='bg-danger text-white'><b>Ker še niste odrasli, je čas spanja za vašo starost premajhen, zato vam priporočamo pogovor z zdravnikom.</b></p>");
			    document.getElementById("skritiNevrologi").style.visibility = "visible";
			  }
			  else if  (casSpanja <= 11) {
			    $("#analizaSpanja").html("Popolno kaj bi hoteli boljšega. Spite v intervalu, ki ga je predpisala Nacionalna fundacija za spanec, to je med <b>devet in enajst ur</b>.");
			  }
			  else {
			    rezultat = 60;
			    $("#analizaSpanja").html("Spali naj bi sicer med <b> 9 in 11 ur </b>, vendar pa dodatna ura spanca ne bo škodila." +
			    "<p class='bg-info text-white'><b>Seveda vam dodatna ura spanca ne bo škodila, vendar pazite, da ne prekoračite zgornje meje, ki je 12 ur</b>.</p>");
			  }
			}
			if (starost > 13 && starost <= 17) {
			  if (casSpanja == 7) {
			    rezultat = 50;
			    $("#analizaSpanja").html("Ste v spodnji kritični meji, saj <b>manj kot 7 ur spanca</b> povzroča razne bolezni." +
			    "<p class='bg-info text-white'><b>Priporočamo, da spite kakšno uro dlje, saj je za vašo starost idealno od osem do deset ur spanca na dan</b>.</p>");
			  }
			  else if (casSpanja < 7) {
			    rezultat = 30;
			    $("#analizaSpanja").html("Stari ste <b>" + starost + "</b>, pa spite le <b>" + casSpanja + "</b>. ur. Če se bo vaša količina spanja s starostjo drastično zmanjševala, se vam bo pa smrt lažje približevala"+
			    "<p class='bg-danger text-white'><b>Priporočamo, da čimprej obiščete zdravnika.</b></p>");
			    document.getElementById("skritiNevrologi").style.visibility = "visible";
			  }
			  else if (casSpanja > 7 && casSpanja <= 10) {
			    $("#analizaSpanja").html("Glede na to, da ste stari <b>" + starost + "</b>, je število ur spanja ravno pravšnje." +
			    "<p class='bg-success text-white'><b>Smo brez besed, vaše spanje je v pravem intervalu, le tako naprej.</b></p>");
			  }
			  else if (casSpanja == 11) {
			    rezultat = 50;
			    $("#analizaSpanja").html("Pri <b>" + starost + ". letih </b> je to že pretirano spanje. Več spanja ne pomeni, da boste zjutraj ravno naspani, ravno obratno. Prekomerno spanje vam lahko celo škoduje." +
			    "<p class='bg-info text-white'><b>Idealno bi bilo spanje med osem in deset ur</b>.</p>");
			  }
			  else if (casSpanja > 11) {
			    rezultat = 60;
			    $("#analizaSpanja").html("Se vidi, da ste v obdobju najstništva. Ste se morda že kdaj spraševali, zakaj ste zjutraj vseeno utrujeni? Naj vam damo odgovor: ker prekomerno spanje učinkuje ravno tako kot prekratko spanje." +
			    "<p class='bg-danger text-white'><b>Probajte spati med osem in deset ur, saj boste tako polni energije, ki jo potrebujete. Ravno tako pa vam priporočamo, da se o svojih spalnih navadah pogovorite z zdravnikom.</b></p>");
			    document.getElementById("skritiNevrologi").style.visibility = "visible";
			  }
			}
			if (starost >= 18 && starost <= 64) {
			  if (casSpanja == 6) {
			    if (prebuditev == 1) {
			      rezultat -= 25;
			    }
			    if (telesnaTeza / (telesnaVisina * telesnaVisina) > 0.0025) {
			      rezultat -= 20;
			      $("#analizaSpanja").html("Glede na to, da ste odrasli, bo šlo nekako, vendar pa ni nič narobe, če podaljšate spanje za kakšno uro." +
			      "<p class='bg-danger text-white'><b>Problem pa smo opazili pri vaši telesni teži. Ne vemo, kako naj vam to povemo, vendar ste predebeli. Poleg tega, da je debelost že sama po sebi nevarna, je nevarna tudi v postelji. S tem ko se uležete, vaša" +
	          " teža pritiska na notranje organe, kar lahko povzroči oteženo dihanje ali pa druge zdravstvene probleme.</b></p>");
	          document.getElementById("skritiNevrologi").style.visibility = "visible";
			    }
			    else {
			      $("#analizaSpanja").html("Glede na to, da ste odrasli, bo šlo nekako, vendar pa ni nič narobe, če podaljšate spanje za kakšno uro." +
			      "<p class='bg-info text-white'><b>Nacionalna fundacija za spanec priporoča spanje med 7 do 9 ur na dan.</b>.</p>");
			    }
			  }
			  if (casSpanja < 6) {
			    if (prebuditev == 1) {
			      rezultat -= 25;
			    }
			    rezultat -= 50;
			    if (telesnaTeza / (telesnaVisina * telesnaVisina) > 0.0025) {
			      rezultat -= 20;
			    }
			    $("#analizaSpanja").html("Nacionalna fundacija za spanec je naredila raziskavo v kateri je odkrila da morajo odrasli spati med <b>6 in 11 ur</b>, vi pa spite <b>le " + casSpanja + ". ur/a/i/e</b>. Prej ali slej boste začutili posledice pomanjkanja spanja." +
			    "<p class='bg-danger text-white'><b>Priporočamo, da se čimprej posvetujete z zdravnikom.</b></p>");
			    document.getElementById("skritiNevrologi").style.visibility = "visible";
        }
			  if (casSpanja > 6 && casSpanja < 11) {
			    if (prebuditev == 1) {
			      rezultat -= 20;
			    }
			    if (telesnaTeza / (telesnaVisina * telesnaVisina) > 0.0025) {
			      rezultat -= 25;
			    }
			    if (rezultat > 75) {
			      $("#analizaSpanja").html("Vaš spanec je popoln, gotovo se vsako juro prebudite polni energije." +
			      "<p class='bg-success text-white'><b>Zdrav življenjski slog + dober spanec = dobro življenje.</b></p>");
			    }
			    else {
			      $("#analizaSpanja").html("Čeprav spite v intervalu, ki ga je predpisala Nacionalna fundacija za spanec, vaš spanec vseeno ni popoln." +
			      "<p class='bg-danger text-white'><b>Problem pa smo opazili pri vaši telesni teži. Ne vemo, kako naj vam to povemo, vendar ste predebeli. Poleg tega, da je debelost že sama po sebi nevarna, je nevarna tudi v postelji. S tem ko se uležete, vaša" +
	          " teža pritiska na notranje organe, kar lahko povzroči oteženo dihanje ali pa druge zdravstvene probleme.</b></p>");
	          document.getElementById("skritiNevrologi").style.visibility = "visible";
			    }
			  }
			  if (casSpanja >= 11) {
			    if (prebuditev == 1) {
			      rezultat -= 20;
			    }
			    if (telesnaTeza / (telesnaVisina * telesnaVisina) > 0.0025) {
			      rezultat -= 25;
			    }
			    $("#analizaSpanja").html("<b>Vaše spalne navade so slabe.</b> Ste se morda že kdaj spraševali, zakaj ste zjutraj vseeno utrujeni? Naj vam damo odgovor: ker prekomerno spanje učinkuje ravno tako kot prekratko spanje." +
			    "<p class='bg-danger text-white'><b>Probajte spati med 7 in 9 ur na dan, saj boste tako polni energije, ki jo potrebujete. Ravno tako pa vam priporočamo, da se o svojih spalnih navadah pogovorite z zdravnikom.</b></p>");
			    document.getElementById("skritiNevrologi").style.visibility = "visible";
			  }
			}
			if (starost > 64) {
			  if (prebuditev == 1) {
			      rezultat -= 25;
			  }
			  if (telesnaTeza / (telesnaVisina * telesnaVisina) > 25) {
			      rezultat -= 20;
			  }
			  if (casSpanja < 5) {
			    $("#analizaSpanja").html("No, čeprav ste že starejši, še ne pomeni, da morate spati manj kot 5 ur na dan. Idealno bi bilo med <b>pet do deset ur na dan</b>." +
			    "<p class='bg-danger text-white'><b>Če še vseeno ne morete spati dlje kot 5 ur, vam priporočamo, da se pogovorite z zdravnikom o svojih težavah.</b></p>");
			    document.getElementById("skritiNevrologi").style.visibility = "visible";
			  }
			  if (casSpanja > 10) {
			    $("#analizaSpanja").html("No saj ste gotovo že oddelali svoje, ne verjamemo pa, da ste toliko utrujeni, da spite <b>" + casSpanja + ". ur dnevno</b>. Ali čez dan nič ne delate, ali pa ste kot panda, ki prespi večino svojega življenja." +
			    "<p class='bg-danger text-white'><b>Priporočamo vam, da se o svojih spalnih navadah posvetujete z zdravnikom.</b></p>");
			    document.getElementById("skritiNevrologi").style.visibility = "visible";
			  }
			  if (casSpanja >=5 && casSpanja <= 10) {
			    if (rezultat > 75) {
			      $("#analizaSpanja").html("Vaš spanec je popoln, gotovo se vsako juro prebudite polni energije." +
			      "<p class='bg-success text-white'><b>Le tako naprej.</b></p>");
			    }
			    else {
			      $("#analizaSpanja").html("Čeprav spite v intervalu, ki ga je predpisala Nacionalna fundacija za spanec, vaš spanec vseeno ni popoln." +
			      "<p class='bg-info text-white'><b>Na kakovost spanca zlasti vpliva vaša teža in sproščenost vašega telesa, ko zaspite. Če želite, se lahko pogovorite z zdravnikom</b>.</p>");
			      document.getElementById("skritiNevrologi").style.visibility = "visible";
			    }
			  }
			}
			merilecSpanca(rezultat);
			
		},
		error: function(err) {
			console.log("Napaka");
		}
	});

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite vse potrebne podatke</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": casSpanja,
		    "vital_signs/body_temperature/any_event/temperature|magnitude": prebuditev
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "Fata"
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'> Podatki so bili uspešno obdelani</span>");
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}

function najdi() {
    var mesto = $("#izberiKraj").val();
    $("#seznamZdravnikov").empty();
    vsiZdravniki = [];
    nevrolog(25, mesto, 1);
}


function nevrolog(specialization,city,page) {

    var response = $.ajax({
        type: "GET",
        headers: {'origin':''},
        url: "https://cors-anywhere.herokuapp.com/http://zdravniki.org/zdravniki?roles=0&specializations="+encodeURIComponent(specialization)+"&page="+page+"&cities="+encodeURIComponent(city),
        success:function(data) {
            var el = $('<div></div>');
            el.html(data);

            el = el.find('.listing');
            var imeZdravnika = el.find('.doctor');
            var podatkiZdravnika = el.find('.specialization');
            var mestoZdravnika = el.find('.workplace');

            for(var i = 0; i < imeZdravnika.length; i++) {
                var doctor = {name: imeZdravnika[i].text,
                              link: $(imeZdravnika[i]).attr('href'),
                              specs: podatkiZdravnika[i].innerText,
                              workplace: mestoZdravnika[i].innerText};
                $("#seznamZdravnikov").append('<div class="col-md-12 doctor"> \
                    <h4><a class="doctor-name"  target="_blank" href="http://zdravniki.org/'+doctor.link+'" id="imeZdravnika">'+doctor.name+'</a></h4> \
                    <a class="specialization">'+doctor.specs+'</a><br> \
                    <a class="workplace">'+doctor.workplace+'</a> \
                </div>');
                vsiZdravniki.push(doctor);
            }
            if(imeZdravnika.length === 0 && vsiZdravniki.length === 0) {
                $("#seznamZdravnikov").empty();
                $("#seznamZdravnikov").append('<div style="text-align: center"><b>Izbrano mesto nima nevrologa ali pa je prišlo do napake.</b><div>');
            } else if(imeZdravnika.length !== 0) {
                nevrolog(specialization,city,page+1);
            }

        }
    });
}

function merilecSpanca(rezultat) {
  const dataSource = {
    chart: {
      caption: "KAKOVOST SPANCA",
      lowerlimit: "0",
      upperlimit: "100",
      theme: "ocean"
    },
    colorrange: {
      color: [
        {
          minvalue: "0",
          maxvalue: "50",
          code: "#F2726F"
        },
        {
          minvalue: "50",
          maxvalue: "75",
          code: "#FFC533"
        },
        {
          minvalue: "75",
          maxvalue: "100",
          code: "#62B58F"
        }
      ]
    },
    dials: {
      dial: [
        {
          value: rezultat,
          tooltext: "do popolnega spanca vam manjka še " + (100 - rezultat) + "%" 
        }
      ]
    },
    trendpoints: {
      point: [
        {
          thickness: "2",
          color: "#E15A26",
          usemarker: "1",
          markerbordercolor: "#E15A26",
          markertooltext: "80%"
        }
      ]
    }
  };
  
  FusionCharts.ready(function() {
    var myChart = new FusionCharts({
      type: "angulargauge",
      renderAt: "chart-container",
      width: "100%",
      height: "623%",
      dataFormat: "json",
      dataSource
    }).render();
  });
}


$(document).ready(function() {
  $('#preveriSpanec').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[1]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[2]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[3]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[4]);
		$("#dodajCasSpanja").val(podatki[5]);
		document.getElementById("prebujanje").checked = false;
		if (podatki[6] == "ja") {
		  document.getElementById("prebujanje").checked = true;
		}
	});
	merilecSpanca(0);
  
});


/* global L, distance */


/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {
  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 12
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Objekt oblačka markerja
  var popup = L.popup();
  
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        json = JSON.parse(xobj.responseText);

        for (var i = 0; i < json.features.length; i++) {

          var ikona = new L.Icon({
            iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-red.png',
            shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
          });

          // Ustvarimo marker z vhodnima podatkoma koordinat 
          // in barvo ikone, glede na tip
          if (i > 95 && i < 99) {
            var marker = L.marker([json.features[i].geometry.coordinates[0][1], json.features[i].geometry.coordinates[0][0]], {icon: ikona});
          }
          else if (i >= 99) {
            var marker = L.marker([json.features[i].geometry.coordinates[1], json.features[i].geometry.coordinates[0]], {icon: ikona});
          }
          else {
            var marker = L.marker([json.features[i].geometry.coordinates[0][0][1], json.features[i].geometry.coordinates[0][0][0]], {icon: ikona});
          }
        
          // Izpišemo želeno sporočilo v oblaček
          if (json.features[i].properties["name"] == undefined) {
            marker.bindPopup("<div>" + '<img src="https://source.medic.clinic/clinic/thumbnail/default.jpg" height="100" width="100" />' + "</div>").openPopup();
          }
          else if (json.features[i].properties["name"] != undefined && json.features[i].properties["addr:street"] == undefined && json.features[i].properties["addr:housenumber"] == undefined) {
            marker.bindPopup("<div>" + json.features[i].properties["name"] + '<img src="https://source.medic.clinic/clinic/thumbnail/default.jpg" height="100" width="100" />' + "</div>").openPopup();
          }
          else if (json.features[i].properties["name"] != undefined && json.features[i].properties["addr:city"] == undefined) {
            marker.bindPopup("<div>" + json.features[i].properties["name"] + "<br/>" + json.features[i].properties["addr:street"] + " " + 
            json.features[i].properties["addr:housenumber"] + "<br/>" +
            '<img src="https://source.medic.clinic/clinic/thumbnail/default.jpg" height="100" width="100" />' + "</div>").openPopup();
          }
          else if (json.features[i].properties["name"] == undefined && json.features[i].properties["addr:city"] != undefined && json.features[i].properties["addr:street"] != undefined) {
            marker.bindPopup("<div>" + json.features[i].properties["addr:street"] + " " + json.features[i].properties["addr:housenumber"] + "<br/>" + ", " +  json.features[i].properties["addr:city"] +
            '<img src="https://source.medic.clinic/clinic/thumbnail/default.jpg" height="100" width="100" />' + "</div>").openPopup();
          }
          else {
            marker.bindPopup("<div>" + json.features[i].properties["name"] + "<br/>" + json.features[i].properties["addr:street"] + " " + 
            json.features[i].properties["addr:housenumber"] + ", " +  json.features[i].properties["addr:city"] + "<br/>" +
            '<img src="https://source.medic.clinic/clinic/thumbnail/default.jpg" height="100" width="100" />' + "</div>").openPopup();
          }
        
          //Dodamo točko na mapo in v seznam
          marker.addTo(mapa);
          markerji.push(marker);
          
          for (var j = 0; j < json.features[i].geometry.coordinates.length; j++) {
            for (var k = 0; k < json.features[i].geometry.coordinates[j].length; k++) {
              var x = json.features[i].geometry.coordinates[j][k][1];
              var y = json.features[i].geometry.coordinates[j][k][0];
              json.features[i].geometry.coordinates[j][k][0] = x;
              json.features[i].geometry.coordinates[j][k][1] = y;
            }
          }
          if (i <= 95) {
             polygon[i] = L.polygon(json.features[i].geometry.coordinates, {color: "green"}).addTo(mapa);
          }
         
        }
    }
  };
  xobj.send(null);
  
  var polygon = [];
  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    for (var i = 0; i < polygon.length; i++) {
      var razdalja = distance(latlng.lat, latlng.lng, json.features[i].geometry.coordinates[0][0][0], json.features[i].geometry.coordinates[0][0][1], "K");
        if (razdalja <= 5) {
           polygon[i].setStyle({color:"green"});
        }
        else {
           polygon[i].setStyle({color:"blue"});
        }
    }
  }

  mapa.on('click', obKlikuNaMapo);
  
});

function dodajPoligon(json) {
  for (var i = 0; i < json.features.length; i++) {
    for (var j = 0; j < json.features[i].geometry.coordinates.length; j++) {
      for (var k = 0; k < json.features[i].geometry.coordinates[j].length; k++) {
        var x = json.features[i].geometry.coordinates[j][k][1];
        var y = json.features[i].geometry.coordinates[j][k][0];
        json.features[i].geometry.coordinates[j][k][0] = x;
        json.features[i].geometry.coordinates[j][k][1] = y;
      }
    }
    var polygon = L.polygon(json.features[i].geometry.coordinates, {color: "green"}).addTo(mapa);
  }
}

function distance(lat1, lon1, lat2, lon2, unit) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    }
    else {
        var radlat1 = Math.PI * lat1/180;
        var radlat2 = Math.PI * lat2/180;
        var theta = lon1-lon2;
        var radtheta = Math.PI * theta/180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        return dist;
    }
}